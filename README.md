# Robot Framework Coding Standard

## 1. Project Structure
    - resources
      - etc
        - run.sh
        - jenkinsfile
      - configs
        - dev
          - env_config.yaml
        - staging
          - env_config.yaml
        - prod
          - env_config.yaml
      - test_data
        - products.yaml
        - translation
          - en.yaml
          - th.yaml
        - user_profile.yaml
      - imports.robot (imports configurations and libraries)
      - common_configs.yaml (All configurations that are used across all environments *e.g.* API endpoint path, timeout)
    - tests (contains subdirectory is acceptable)
      - mobile
        - e2e
          - company.robot
          - guest.robot
          - individual_email.robot
          - individual_facebook.robot
        - features
          - login (feature)
            - customer.robot (subfeature)
            - agent.robot (subfeature)
          - payment
            - barcode_test.robot
      - web
        - e2e
          - company.robot
          - guest.robot
          - individual_email.robot
          - individual_facebook.robot
        - features
          - login (feature)
            - customer.robot (subfeature)
            - agent.robot (subfeature)
          - payment
            - barcode_test.robot
    - keywords (contains subdirectory is optional)
      - api
        - login_keywords.robot (follow api resource)
      - web
        - features
          - login_keywords.robot
          - welcome_keywords.robot
        - pages (pages file will include declare variable locator)
          - login_page.robot
          - register_page.robot
      - ios
      - android
    - requirements.txt
    - README.md (getting started, how to execute tests, documentation)

---

## 2. Test Environments
- Test environment **MUST** be isolated using virtualenv
- Test configuration file (variable file) can override with "YAML" format (pyyaml is needed to install)
- requirements.txt should specifies library versions

*dev/env_config.yaml*

    api_host: https://alpha-sample.dev.com
    db_host: alpha-db.dev.com

*staging/env_config.yaml*

    api_host: https://sample.dev.com
    db_host: db.dev.com

*requirements.txt*

    roboframework==3.0.2
    robotframework-selenium2library==1.8.0
    robotframework-requests>=0.4.6
    requests>=2.13.0

---

## 3. Source file basics
### File name
- Short and descriptive
- No 'robot' in test suite name
- Text based format with file extension **".robot"**
- File and folder names should be **'lowercase'** with **'underscore'** separator (snake_case). For example **register_test.robot, register_keywords.robot**

| Example           | Result   |
| ------------------|:--------:|
| login_test.robot  | Good |
| login_robot.robot | Bad |
| loginTest.robot   | Bad |

### File encoding: UTF-8
- Source files are encoded in **UTF-8**

### Whitespace characters
Aside from the line terminator sequence, the **ASCII horizontal space character (0x20)** is the only whitespace character that appears anywhere in a source file. This implies that:

1. All other whitespace characters in string and character literals are escaped.
2. Tab characters are **NOT** used for indentation.
3. Config your editor to translate Tab to 4 spaces

### Line terminator sequence
The line terminator sequence characters must be \n

---

## 4. Source file structure
### Keywords
A keyword file consists of, **in order**:

1. Settings
2. Variables
3. Keywords

### Testcases
A test case file consists of, **in order**:

1. Settings
2. Variables
3. Test Cases
4. Keywords (Exception for setup, teardown, and data-driven only)

### Configurations
A configuration file must be **yaml** format
**Note:** Include variable file .yaml must use 'Variables' in 'Settings' section see example below

    *** Settings ***
    Variables          env_config.yaml

---

## 5. Formatting
### Indentation
To align with Python standard code style, any indentation and space separators are done by using 4 whitespace characters only.

### Column limit
A line should not contain more than 150 characters

### Line-wrapping
There is no comprehensive, deterministic formula showing exactly how to line-wrap in every situation.

### Vertical Whitespace
Apart from separating each keywords, testcases, or sections, no extra empty line should be allowed

### Horizontal Whitespace
No trailing spaces are allowed

### Horizontal Alignment
This practice is permitted, but is never required. It is not even required to maintain horizontal alignment in places where it was already used.

### Variable Declaration
- Use snake_case for all variables whether it is local variable or global variable (configurations)
- Exceptions are pre-defined variable *e.g.* ${SPACE}, ${CURDIR}
- Nested variables declaration is not allowed except for accessing value inside dictionary or list

### Comments
- Comment Keywords (Comment|[description]) and '#' are both allowed
- Comment should be the description of the overall logic that the following keywords are doing

---

## 6. Naming

### Test Cases
- Capitalized only first letter of Test cases name.
- Do not use dot in the test case name
- Test case name should not be too long

    | Example                                               | Result   |
    | ------------------------------------------------------|:--------:|
    | Login Success | Good|
    | User login to website with correct username & password, then the welcome page should be displayed.| Bad (Too detail)|

### Keywords
- Naming: Capitalized only first letter of Keywords name.
    
    For example:
    
        Login with valid credentials
        
- Separator: **FOUR SPACES** separated between keywords and arguments. If arguments contain white spaces, it must be escaped with **${SPACE}**

### Variables
- Naming: descriptive but not too long.
- should be in snake_case
- Page object locator should starts with element type in 3 characters
  - Example
    - txt_username
    - rdo_gender
    - chk_enable
    - lbl_province
    - btn_submit

### Resources
Variable declare in resource directory (configurations) should follow normal variable naming convention

### Test Library
Follow Google python coding standard
https://github.com/google/styleguide/blob/gh-pages/pyguide.md

---

## 7. Programming Practices

### Test Cases
- Documentation and Tags must be added for all test cases (default tags at test suite are acceptable)
- Tests should be independent. Initialization using setup/teardown.
- Test steps are understandable.
- One test case should be testing one thing.
- No complex logic (LOOPS, IF/ELSE) on test case level
- Preferably less than 10 steps.
- Select suitable abstraction level.
- Parameter validation tests should be covered by unit test. If you really want to do the parameter validation tests in acceptance testing, consider [data-driven](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#data-driven-style) test cases design.
- Try: [Gherkin](https://cucumber.io/docs/gherkin/) syntax

**Good**: Appropriate abstraction level

    *** Test Cases ***
    Login To Wallet Application - Success
        [Documentation]    User specify correct username and password should be login to Wallet successfully, then welcome page is displayed.
        [Tags]    login    ready    regression    testrailid:C00020
        Given User specify username 'test_username' and password 'test_password'
        When User press login button.
        Then Welcome page should be display.
    
**Bad**: Too low abstraction level

    *** Test Cases ***
    Login To Wallet Application - Success
        [Documentation]    User specify correct username and password should be login to Wallet successfully, then welcome page is displayed.
        [Tags]    login    ready    regression    testrailid:C00020
        Input text   id=input_username    test_username
        Input text   id=input_password    test_password
        Click button    id=submitButton
        Wait until element is visible    id=welcome_div    ${selenium_timeout}
        ${welcome_text}=    Get text    id=welcome_div
        Should be equal as strings    ${welcome_text}    Welcome Page!

### Keywords
- Should explain **WHAT** keyword does, not how does its task.
- Different abstraction levels. For example *Input Text* or *Administrator Logs Into System*
- Keyword should not have more than 5 arguments, otherwise, consider using dictionary as an argument.
- All keywords must be documented.

**Good:**

    *** Keywords ***
    Process Start
        [Documentation]    Start the application process using the script 'startup.sh'
        Run process    /data/projects/sample/startup.sh

**Bad:**

    *** Keywords ***
    Enter To The Directory And Execute The Script To Start Process
        Run process    /data/projects/sample/startup.sh

- **AVOID**
    - **Log To Console** and **Log** keywords. Use robot DEBUG file option to debug keywords and variables *(robot -b debug.log login_test.robot)*
    - Use **polling** not **sleeping** to synchronize tasks.
    - Do not use **Set Global Variable** keyword
    - **Close All Browsers** with PhantomJS
        - [GhostDriver](https://github.com/detro/ghostdriver) is a webdriver for [PhantomJS](http://phantomjs.org/) lacks of new development for the last 2 years and it cannot guarantee the compatibility with new version of PhantomJS. There is an issue with multiple session handling for GhostDriver. If you use the keyword **Close All Browsers** It could make your tests fail randomly and may impact with other tests on CI platform. So please **AVOID** using it!!
    - UI testing should not Open Browser and Close Browser in every test case. They should be put in suite setup and teardown. Test case teardown should reset the state back to the first state so that every test case starts at the same state.
    - Python keywords, if reusable, should consider make them a library.
        
**Good:**

    *** Keywords ***
    Push Message To Queue
        [Documentation]    Push message to queue and wait until app fetch message from queue.
        [Arguments]    ${message}
        ${response}=    Push message    ${queue_host}    ${message}
        Wait until keyword succeeds    ${retry_timeout}    ${retry_interval}    Message should be fetched from queue
        
**Bad:**

    *** Keywords ***
    Push Message To Queue
        [Documentation]    Push message to queue and wait until app fetch message from queue.
        [Arguments]    ${message}
        ${response}=    Push message    ${queue_host}    ${message}
        Sleep    30s
        Message should be fetched from queue
       
**Good:**

    *** Settings ***
    Suite Teardown    Close browser
    
**Bad:**

    *** Settings ***
    Suite Teardown    Close all browsers

### Variables
- Overided variables should be taken into YAML file. YAML file can be parsed as 'scalar ${}' , 'list @{}' and 'dictionary &{}'

**Good:** Put complex JSON post body into YAML file register_user.yaml --> YAML file can be parsed into dictionary object (JSON)

    headers:
        content-type: application/json
    register_json:
        username: myusername
        password: mypassword
        email: email@central.com
        address:
            - Huaykwang
            - Bangkok
            - Thailand
            - 10310

**register_keyword.robot**

    *** Settings ***
    Library        RequestsLibrary
    Variables      register_user.yaml

    *** Keyword ***
    Register Account
        [Documentation]    Register new account based on 
        Create Session    registerNewAcct    ${host}     headers=${headers}
        ${response}=    Post request    registerNewAcct    data=${register_json}
        Should be equal as strings    ${response.status_code}    200
        
- Consider built-in variables

| Variable  | Explanation   |
| ----------|---------------|
|${CURDIR}  |An absolute path to the directory where the test data file is located. This variable is case-sensitive.
|${TEMPDIR} |An absolute path to the system temporary directory. In UNIX-like systems this is typically /tmp, and in Windows c:\Documents and Settings\<user>\Local Settings\Temp.
|${EXECDIR} |An absolute path to the directory where test execution was started from.
|${/}     |The system directory path separator. / in UNIX-like systems and \ in Windows.
|${:}     |The system path element separator. : in UNIX-like systems and ; in Windows.
|${\n}      |The system line separator. \n in UNIX-like systems and \r\n in Windows. New in version 2.7.5.
|${SPACE} |Escaped space character.
|${EMPTY} |Escaped empty string.
|${None}  |None type object.
|${True}  |Boolean True
|${False} |Boolean False

And other automatic variables [http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#automatic-variables](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#automatic-variables)

### Resources
- Use single resource file for common libraries or resources. Remove redundant imports.

*web_imports.robot*

    Library     RequestsLibrary
    Library     Collections
    Library     DateTime
    Library     Selenium2Library
    Resource    keywords/common_keywords.robot
    Variables   resources/${ENV}/env_config.yaml
    
*register_test.robot*

    *** Settings ***
    Resource    ../resources/imports.robot

    *** Test Cases ***
    Test number one

### Tags
- If all test cases in the test suite have the same tag, default tags at test suite should be used instead
- Each test case should tag as follows:
  - Testrail case id 1 RBFW testcase can have multiple Testrail case id*e.g.* testrailid:C00020  testrailid:C00021
  - feature that test case would verity *e.g.* login, register and so on
  - test case priority *e.g.* high, medium and low
  - **<optional>** suite type *e.g.* smoke, sanity or regression
  - **<jira issue>** e.g. jira:OFM-2223 jira:OFM-4456

---

## 8. Documentation
- Test case objective must be put in the [documentation] for that test case
- Keywords must have a documentation that describes what the keyword does and the details of the arguments
- Test suite documentation should contain background information, why tests are created, notes about execution environment, etc.

**Good:**

    *** Settings ***
    Documentation    Tests to verify that account withdrawals succeed and
    ...              fail correctly depending from users account balance
    ...              and account type dependent rules.
    ...              See http://internal.example.com/docs/abs.pdf

## 9. Tools
- [robot.tidy](http://robot-framework.readthedocs.io/en/3.0/_modules/robot/tidy.html) - Source Code Formatter
- [time2csv](https://bitbucket.org/robotframework/robottools/src/master/times2csv/) - Profiling tool to track time used by each keywords

## 10. Page object locator variable pattern
| Locator       | Abbreviation | Example         |
| --------------|--------------|-----------------|
| Text box      | txt          | txt_username    |
| Text area     | txa          | txa_description |
| Button        | btn          | btn_submit      |
| Label         | lbl          | lbl_user_id     |
| Dropdown list | ddl          | ddl_province    |
| Image         | img          | img_profile     |
| Link          | lnk          | lnk_user_detail |
| Table         | tbl          | tbl_user_list   |

## 11. Test script desing principles
- Do not Repeat Yourself (DRY), a basic principle of software development aimed at reducing repetition of information. The DRY principle is stated as, "Every piece of knowledge or logic must have a single, unambiguous representation within a system."

**Bad 1:**

    *** Test Cases ***
        Verify All Element Is Visible
        SeleniumLibrary.Wait Until Element Is Visible      css=id=btn-login
        SeleniumLibrary.Wait Until Element Is Visible      css=id=txt-user
        SeleniumLibrary.Wait Until Element Is Visible      css=id=txt-password
    
**Good 1:**

    *** Keywords ***
    Verify That Web Elements Are Visible
        [Arguments]     @{elems}
        FOR    ${elem}    IN    @{elems}
        SeleniumLibrary.Wait Until Element Is Visible    ${elem}    timeout=${GLOBALTIMEOUT}
        END
    
    *** Test Cases ***
    Verify That All Elements Are Visible
        Verify That Web Elements Are Visible    css=id=btn-login      css=id=txt-user      css=id=txt-password

**Bad 2:**

    *** Test Cases ***
    Verify That Login Page Is Accessible
        Open Browser    http://www.something.com/login.html     browser=chrome
        Maximize Browser Window

    Verify That User Can Login
        Open Browser    http://www.something.com/login.html     browser=chrome
        Maximize Browser Window
        ...
    
**Good 2:**

    *** Keywords ***
    Open Login Page
        Open Browser    http://www.something.com/login.html     browser=chrome
        Maximize Browser Window
    
    *** Test Cases ***
    Verify That Login Page Is Accessible
        Open Login Page

    Verify That User Can Login
        Open Login Page
        ...

- Keep It Stupid Simple (KISS) keep the code simple and clear, making it easy to understand. After all, programming languages are for humans to understand — computers can only understand 0 and 1 — so keep testscript simple and straightforward. Keep your keywords small. Each keyword should never be more than 10-20 lines.
Each keyword should only solve one small problem, not many use cases. If you have a lot of conditions in the keyword, break these out into smaller keywords. It will not only be easier to read and maintain, but it can help find bugs a lot faster.

- Page object model (POM)
