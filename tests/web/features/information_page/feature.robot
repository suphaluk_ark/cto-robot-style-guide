*** Setting ***
Resource    ${CURDIR}/../../resources/imports.robot
Resource    ${CURDIR}/../../keywords/web/feature/login_keywords.robot
Variables   ${CURDIR}/../../resources/testdata/${ENV}/test_data.yaml
Suite Setup    Run Keywords    Open browser    ${central_url}    gc    AND    Maximize Browser Window
Suite Teardown    Close browser

*** Variables ***
${user_a}    a
${password}    pass

*** Test Cases ***
TWC_EDM_00001 Login Fail
    [Documentation]    To verify login on web fail with account not found
    [Setup]    Run Keyword And Ignore Error    Close Popup
    Click My Account
    Input username    QA_TEST
    Input password    password
    Click login button
    Check error label appear    ข้อมูลในการ Login ไม่ถูกต้อง
    Capture Page Screenshot
    # Login web    ${user_a}    ${password}

*** Keyword ***
Close Popup
    Click Element    css=.close