*** Setting ***
Resource    ${CURDIR}/../../../resources/imports.robot

*** Variables ***
${txt_username}    css=#email
${txt_password}    css=#password
${btn_login}    css=#btnSignIn
${lbl_error_message}    css=#Message

*** Keywords ***
Input username
    [Documentation]    Input username to username text box
    [Arguments]    ${username}
    Input text    ${txt_username}    ${username}

Input password
    [Documentation]    Input password to password text box
    [Arguments]    ${password}
    Input text    ${txt_password}    ${password}

Click login button
    [Documentation]    Click login button
    Click Element    ${btn_login}

Check error label appear
    [Arguments]    ${message}
    [Documentation]    Assert error message on error label
    Wait Until Element Is Visible    ${lbl_error_message}
    Element Text Should Be    ${lbl_error_message}    ${message}
