*** Setting ***
Library      RequestsLibrary
Library      Collections
Library      DateTime
Library      SeleniumLibrary
Resource     ./web/pages/homepage.robot
Resource     ./qa-common/CommonKeywords.robot
Resource     ./qa-common/CommonWebKeywords.robot
Variables    ../resources/configs/${ENV}/env_config.yaml
Variables    ../resources/test_data/products.yaml
Variables    ../resources/configs/common.yaml