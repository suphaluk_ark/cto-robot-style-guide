*** Setting ***
Resource    ${CURDIR}/../../../resources/imports.robot

*** Variables ***
${lbl_welcome}    id=welcome

*** Keywords ***

Click My Account
    Click Element    css=a[data-title='Login']:contains('บัญชีของคุณ')

Check go to welcome page
    [Documentation]    Check welcome page show.
    Should contain element    id=welcome